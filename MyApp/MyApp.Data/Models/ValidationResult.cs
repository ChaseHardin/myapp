namespace MyApp.Data.Models
{
    public class ValidationResult
    {
        public ValidationResult() { }

        public ValidationResult(string property, string message, params object[] formatObjects)
        {
            Property = property;
            Message = (formatObjects != null && formatObjects.Length > 0)
                ? string.Format(message, formatObjects)
                : message;
        }

        public string Property { get; set; }
        public string Message { get; set; }
    }
}