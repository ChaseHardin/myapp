using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace MyApp.Data.Models
{
    public class BaseModel
    {
        public BaseModel()
        {
            ValidationResults = new List<ValidationResult>();
        }

        [IgnoreDataMember]
        public bool ItemNotFound { get; set; }
        [IgnoreDataMember]
        public bool ItemCreated { get; set; }

        [IgnoreDataMember]
        public List<ValidationResult> ValidationResults { get; set; }

        public void AddValidation(string property, string message, params object[] formatInputs)
        {
            ValidationResults.Add(new ValidationResult(property, message, formatInputs));
        }
    }
}
