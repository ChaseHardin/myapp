using MyApp.Data.Infrastructure;
using MyApp.Data.Models;

namespace MyApp.Data.Repositories
{
    public class EmployeeRepository : BaseRepository<Employee>, IEmployeeRepository
    {
        public EmployeeRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }
    }

    public interface IEmployeeRepository : IBaseRepository<Employee>
    {
    }
}