using System.Data.Entity;
using MyApp.Data.Models;

namespace MyApp.Data
{
    public class MyAppEntities : DbContext
    {
        public MyAppEntities() : base("MyAppEntities") { }

        public DbSet<Employee> Employees { get; set; }

        public virtual void Commit()
        {
            SaveChanges();
        }
    }
}