namespace MyApp.Data.Infrastructure
{
    public class DbFactory : Disposable, IDbFactory
    {
        MyAppEntities dbContext;

        public MyAppEntities Init()
        {
            return dbContext ?? (dbContext = new MyAppEntities());
        }

        protected override void DisposeCore()
        {
            if (dbContext != null)
                dbContext.Dispose();
        }
    }

    public interface IDbFactory
    {
        MyAppEntities Init();
    }
}
