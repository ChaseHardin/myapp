using MyApp.Data.Infrastructure;
using MyApp.Data.Models;
using MyApp.Data.Repositories;

namespace MyApp.Business.Services
{
    public class EmployeeService : IEmployeeService
    {
        private readonly IEmployeeRepository _employeeRepository;
        private readonly IUnitOfWork _unitOfWork;

        public EmployeeService(IUnitOfWork unitOfWork, IEmployeeRepository employeeRepository)
        {
            _unitOfWork = unitOfWork;
            _employeeRepository = employeeRepository;
        }

        public Employee GetEmployee(int id)
        {
            return _employeeRepository.GetById(id);
        }

        public void SaveEmployee()
        {
            _unitOfWork.Commit();
        }

        public Employee AddEmployee(Employee employee)
        {
            return _employeeRepository.Add(employee);
        }
    }

    public interface IEmployeeService
    {
        Employee GetEmployee(int id);
        void SaveEmployee();
        Employee AddEmployee(Employee employee);
    }
}
