﻿using System.Web.Http;
using MyApp.Business.Services;
using MyApp.Data.Models;

namespace MyApp.Web.Controllers
{
    public class EmployeeController : BaseController
    {
        private readonly IEmployeeService _employeeService;

        public EmployeeController(IEmployeeService employeeService)
        {
            _employeeService = employeeService;
        }

        [HttpPost]
        public IHttpActionResult AddEmployee([FromBody] Employee employee)
        {
            if (ModelState.IsValid)
            {
                _employeeService.AddEmployee(employee);
                _employeeService.SaveEmployee();

                return Ok(_employeeService.AddEmployee(employee));
            }

            return BadRequest("Invalid Entry");
        }
    }
}